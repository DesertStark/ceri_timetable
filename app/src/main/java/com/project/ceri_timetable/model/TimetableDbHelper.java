package com.project.ceri_timetable.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimetableDbHelper extends SQLiteOpenHelper {

    private static TimetableDbHelper instance;

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "timetable.db";
    public static final String TABLE_NAME = "vevent";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DATE_START = "dtStart";
    public static final String COLUMN_DATE_END = "dtEnd";
    public static final String COLUMN_TIME_START = "timeStart";
    public static final String COLUMN_TIME_END = "timeEnd";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_SUMMARY = "summary";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_PROF = "prof";

    private SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.FRANCE);
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);

    private String ignoreCancelledAndHoliday = COLUMN_SUMMARY + " NOT LIKE '%Annulation%' AND " + COLUMN_SUMMARY + " NOT LIKE '%Férié%'";


    public TimetableDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void init(Context context) {
        instance = new TimetableDbHelper(context);
    }

    public static TimetableDbHelper get() {
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                TABLE_NAME,
                _ID,
                COLUMN_NAME,
                COLUMN_DATE_START,
                COLUMN_DATE_END,
                COLUMN_TIME_START,
                COLUMN_TIME_END,
                COLUMN_DATE,
                COLUMN_SUMMARY,
                COLUMN_LOCATION,
                COLUMN_DESCRIPTION,
                COLUMN_TYPE,
                COLUMN_PROF
                ));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addVEvent(VEvent vEvent) {
        SQLiteDatabase db = this.getWritableDatabase();
        long rowID = 0;
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, vEvent.getName());
        cv.put(COLUMN_DATE_START, dateTimeFormatter.format(vEvent.getDtStart()));
        cv.put(COLUMN_DATE_END, dateTimeFormatter.format(vEvent.getDtEnd()));
        cv.put(COLUMN_TIME_START, timeFormatter.format(vEvent.getDtStart()));
        cv.put(COLUMN_TIME_END, timeFormatter.format(vEvent.getDtEnd()));
        cv.put(COLUMN_DATE, dateFormatter.format(vEvent.getDtStart()));
        cv.put(COLUMN_SUMMARY, vEvent.getSummary());
        cv.put(COLUMN_LOCATION, vEvent.getLocation());
        cv.put(COLUMN_DESCRIPTION, vEvent.getDescription());
        cv.put(COLUMN_TYPE, vEvent.getType());
        cv.put(COLUMN_PROF, vEvent.getProf());
        rowID = db.insert(TABLE_NAME, null, cv);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    public void addVEvents(ArrayList<VEvent> vEvents) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        db.beginTransaction();
        for (VEvent vEvent : vEvents) {
            cv.put(COLUMN_DATE_START, dateTimeFormatter.format(vEvent.getDtStart()));
            cv.put(COLUMN_DATE_END, dateTimeFormatter.format(vEvent.getDtEnd()));
            cv.put(COLUMN_SUMMARY, vEvent.getSummary());
            cv.put(COLUMN_LOCATION, vEvent.getLocation());
            cv.put(COLUMN_DESCRIPTION, vEvent.getDescription());
            cv.put(COLUMN_TYPE, vEvent.getType());
            db.insert(TABLE_NAME, null, cv);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void refreshVEvents(ArrayList<VEvent> vEvents) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        db.beginTransaction();
        db.delete(TABLE_NAME, null, null);
        for (VEvent vEvent : vEvents) {
            cv.put(COLUMN_NAME, vEvent.getName());
            cv.put(COLUMN_DATE_START, dateTimeFormatter.format(vEvent.getDtStart()));
            cv.put(COLUMN_DATE_END, dateTimeFormatter.format(vEvent.getDtEnd()));
            cv.put(COLUMN_TIME_START, timeFormatter.format(vEvent.getDtStart()));
            cv.put(COLUMN_TIME_END, timeFormatter.format(vEvent.getDtEnd()));
            cv.put(COLUMN_DATE, dateFormatter.format(vEvent.getDtStart()));
            cv.put(COLUMN_SUMMARY, vEvent.getSummary());
            cv.put(COLUMN_LOCATION, vEvent.getLocation());
            cv.put(COLUMN_DESCRIPTION, vEvent.getDescription());
            cv.put(COLUMN_TYPE, vEvent.getType());
            cv.put(COLUMN_PROF, vEvent.getProf());
            db.insert(TABLE_NAME, null, cv);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    /*public int updateVEvent(VEvent vEvent) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_DATE_START, dateTimeFormatter.format(vEvent.getDtStart()));
        cv.put(COLUMN_DATE_END, dateTimeFormatter.format(vEvent.getDtEnd()));
        cv.put(COLUMN_SUMMARY, vEvent.getSummary());
        cv.put(COLUMN_LOCATION, vEvent.getLocation());
        cv.put(COLUMN_DESCRIPTION, vEvent.getDescription());
        cv.put(COLUMN_TYPE, vEvent.getType());
        int res = db.update(TABLE_NAME, cv, String.format("%s=%s", _ID, Long.toString(vEvent.getId())), null);
        db.close();
        return res;
    }*/

    public Cursor fetchAllVEvents() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Cursor fetchNextVEvents() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -15);
        String sLateDate = dateTimeFormatter.format(calendar.getTime());
        cursor = db.query(TABLE_NAME, null, COLUMN_DATE_START + " >= ? AND " + ignoreCancelledAndHoliday, new String[] {sLateDate}, null, null, COLUMN_DATE_START, "2");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Cursor fetchVEventsOfTheDay(Date date) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
        String sDate = formatter.format(date);
        cursor = db.query(TABLE_NAME, null, COLUMN_DATE_START + " LIKE ? AND " + ignoreCancelledAndHoliday, new String[] {sDate + "%"}, null, null, COLUMN_DATE_START);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Cursor fetchEvaluations() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        cursor = db.query(TABLE_NAME, null, COLUMN_TYPE + " = ?", new String[] {"Evaluation"}, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Cursor fetchNextEvaluations() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        String currentDate = dateTimeFormatter.format(Calendar.getInstance().getTime());
        cursor = db.query(TABLE_NAME, null, COLUMN_TYPE + " = ? AND " + COLUMN_DATE_START + " > ? AND " + ignoreCancelledAndHoliday, new String[] {"Evaluation", currentDate}, null, null, COLUMN_DATE_START);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public void resetVEvents() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

    public static VEvent cursorToVEvent(Cursor cursor) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
        try {
            VEvent vEvent = new VEvent();
            vEvent.setId(Long.parseLong(cursor.getString(cursor.getColumnIndex(_ID))));
            vEvent.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
            vEvent.setDtStart(formatter.parse(cursor.getString(cursor.getColumnIndex(COLUMN_DATE_START))));
            vEvent.setDtEnd(formatter.parse(cursor.getString(cursor.getColumnIndex(COLUMN_DATE_END))));
            vEvent.setSummary(cursor.getString(cursor.getColumnIndex(COLUMN_SUMMARY)));
            vEvent.setLocation(cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)));
            vEvent.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
            vEvent.setType(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)));
            vEvent.setProf(cursor.getString(cursor.getColumnIndex(COLUMN_PROF)));
            return vEvent;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}