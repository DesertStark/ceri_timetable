package com.project.ceri_timetable.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VEvent {
    private long id;

    private String name;
    private Date dtStart;
    private Date dtEnd;
    private String summary;
    private String location;
    private String description;
    private String type;
    private String prof;

    public VEvent() {
    }

    public VEvent(Date dtStart, Date dtEnd, String summary, String location, String description) {
        this.dtStart = dtStart;
        this.dtEnd = dtEnd;
        this.summary = summary;
        this.location = location;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDtStart() {
        return dtStart;
    }

    public String getDisplayableDtStart() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE);
        return formatter.format(dtStart);
    }

    public void setDtStart(Date dtStart) {
        this.dtStart = dtStart;
    }

    public Date getDtEnd() {
        return dtEnd;
    }

    public String getDisplayableDtEnd() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE);
        return formatter.format(dtEnd);
    }

    public void setDtEnd(Date dtEnd) {
        this.dtEnd = dtEnd;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }

    @Override
    public String toString() {
        return "VEvent{" +
                "dtStart=" + dtStart +
                ", dtEnd=" + dtEnd +
                ", summary='" + summary + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
