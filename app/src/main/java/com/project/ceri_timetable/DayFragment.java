package com.project.ceri_timetable;


import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.project.ceri_timetable.model.TimetableDbHelper;
import com.project.ceri_timetable.model.VEvent;
import com.project.ceri_timetable.util.Toaster;
import com.project.ceri_timetable.webService.IcalParser;
import com.project.ceri_timetable.webService.WebServiceUrl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class DayFragment extends Fragment {

    private View view;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private FloatingActionButton calendarBtn;

    private Calendar calendar;
    private Date selectedDate;
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);


    public DayFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment DayFragment.
     */
    public static DayFragment newInstance() {
        return new DayFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_day, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Agenda");
        listView = view.findViewById(R.id.listView);
        calendar = Calendar.getInstance();
        selectedDate = Calendar.getInstance().getTime();
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year, month, dayOfMonth);
                selectedDate = calendar.getTime();
                ((MainActivity) getActivity()).getSupportActionBar().setTitle("Agenda du " + formatter.format(selectedDate));
                refreshList(listView, selectedDate);
            }
        };
        calendarBtn = view.findViewById(R.id.calendarBtn);
        calendarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.setTime(selectedDate);
                new DatePickerDialog(view.getContext(),
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Agenda du " + formatter.format(selectedDate));
        refreshList(listView, selectedDate);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new IcalTask(DayFragment.this).execute();
            }
        });
    }

    public void refreshList(ListView listView, Date date) {
        /*SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(getContext(),
                android.R.layout.simple_list_item_2,
                TimetableDbHelper.get().fetchVEventsOfTheDay(date),
                new String[] {TimetableDbHelper.COLUMN_DATE_START, TimetableDbHelper.COLUMN_SUMMARY},
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);*/
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(getContext(),
                R.layout.row2,
                TimetableDbHelper.get().fetchVEventsOfTheDay(date),
                new String[] {
                        TimetableDbHelper.COLUMN_TIME_START,
                        TimetableDbHelper.COLUMN_TIME_END,
                        TimetableDbHelper.COLUMN_DATE,
                        TimetableDbHelper.COLUMN_NAME,
                        TimetableDbHelper.COLUMN_TYPE,
                        TimetableDbHelper.COLUMN_LOCATION,
                        TimetableDbHelper.COLUMN_PROF,
                },
                new int[] {
                        R.id.timeBegin,
                        R.id.timeEnd,
                        R.id.date,
                        R.id.name,
                        R.id.type,
                        R.id.location,
                        R.id.prof,
                }, 0);
        listView.setAdapter(cursorAdapter);
    }

    private static class IcalTask extends AsyncTask {
        private WeakReference<DayFragment> fragmentReference;
        private DayFragment fragment;
        private Exception exception = null;
        private ArrayList<VEvent> vEvents = new ArrayList<>();

        public IcalTask(DayFragment context) {
            fragmentReference = new WeakReference<>(context);
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                URL url = WebServiceUrl.getURL();
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream is = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                IcalParser icalParser = new IcalParser(reader);
                VEvent vEvent;
                TimetableDbHelper.get().resetVEvents();
                while ((vEvent = icalParser.nextVEvent()) != null) {
                    vEvents.add(vEvent);
                }
            } catch (UnknownHostException e) {
                exception = e;
            } catch (MalformedURLException e) {
                WebServiceUrl.resetUrl();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (fragmentReference.get() != null && fragmentReference.get().getActivity() != null) {
                fragment = fragmentReference.get();
                if (exception instanceof UnknownHostException) {
                    Toaster.noConnection(fragment.getContext());
                } else {
                    TimetableDbHelper.get().refreshVEvents(vEvents);
                    ListView listView = fragment.view.findViewById(R.id.listView);
                    fragment.refreshList(listView, fragment.selectedDate);
                }
                fragment.swipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}
