package com.project.ceri_timetable.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.project.ceri_timetable.MainActivity;
import com.project.ceri_timetable.R;
import com.project.ceri_timetable.model.TimetableDbHelper;
import com.project.ceri_timetable.model.VEvent;

import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver {

    public AlarmReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        // create notification here
        Cursor nextEvaluations = TimetableDbHelper.get().fetchNextEvaluations();
        nextEvaluations.moveToFirst();
        VEvent vEvent = TimetableDbHelper.cursorToVEvent(nextEvaluations);
        if (vEvent != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, 7);
            Calendar event = Calendar.getInstance();
            event.setTime(vEvent.getDtStart());
            if (event.before(calendar)) {
                NotificationHelper notificationHelper = new NotificationHelper(context);
                NotificationCompat.Builder builder = notificationHelper.getNotification("Évaluation", "Moins d'une semaine avant l'evaluation " + vEvent.getName());
                notificationHelper.getManager().notify(1, builder.build());
//                notification("Evaluation", "Plus qu'une semaine avant l'evaluation " + vEvent.getName());
                Log.d("ALARM", "ALARM");
            }
        }
    }
}