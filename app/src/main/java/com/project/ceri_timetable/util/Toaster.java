package com.project.ceri_timetable.util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class Toaster {

    public static void noConnection(Context context) {
        Toast toast = Toast.makeText(context, "Impossible de récupérer les informations pour le moment", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 100);
        toast.show();
    }
}
