package com.project.ceri_timetable;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.project.ceri_timetable.model.TimetableDbHelper;
import com.project.ceri_timetable.model.VEvent;
import com.project.ceri_timetable.util.AlarmReceiver;
import com.project.ceri_timetable.util.NotificationHelper;
import com.project.ceri_timetable.util.Toaster;
import com.project.ceri_timetable.webService.IcalParser;
import com.project.ceri_timetable.webService.WebServiceUrl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TimetableDbHelper.init(this);

        final SharedPreferences sharedPreferences = this.getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String url = sharedPreferences.getString("url", null);
        if (url != null) {
            WebServiceUrl.setUrlStr(url);
        }

        new IcalTask(this).execute();

        bottomNavigationView = findViewById(R.id.activity_main_bottom_navigation);

        changeFragment(new HomeFragment());

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_home:
                        changeFragment(HomeFragment.newInstance());
                        return true;
                    case R.id.action_day:
                        changeFragment(DayFragment.newInstance());
                        return true;
                    case R.id.action_evaluations:
                        changeFragment(EvaluationFragment.newInstance());
                        return true;
                    default:
                        MainActivity.super.onOptionsItemSelected(menuItem);
                        return false;
                }
            }
        });
        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                // Do nothing
            }
        });

        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_buttons, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                // User chose the "Settings" item, show the app settings UI...
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(intent, 1);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        new IcalTask(this).execute();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
        if (fragment instanceof HomeFragment) {
            super.onBackPressed();
        } else {
            changeFragment(new HomeFragment());
        }
    }

    private void changeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragment, null).commit();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private static class IcalTask extends AsyncTask {
        private WeakReference<MainActivity> activityReference;
        Exception exception = null;
        ArrayList<VEvent> vEvents = new ArrayList<>();

        public IcalTask(MainActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                URL url = WebServiceUrl.getURL();
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream is = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                IcalParser icalParser = new IcalParser(reader);
                VEvent vEvent;
                while ((vEvent = icalParser.nextVEvent()) != null) {
                    vEvents.add(vEvent);
                }
            } catch (UnknownHostException e) {
                exception = e;
            } catch (MalformedURLException e) {
                WebServiceUrl.resetUrl();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (activityReference.get() != null) {
                Fragment fragment = activityReference.get().getSupportFragmentManager().findFragmentById(R.id.frame);
                if (exception instanceof UnknownHostException) {
                    Toaster.noConnection(activityReference.get());
                } else {
                    TimetableDbHelper.get().refreshVEvents(vEvents);
                    ListView listView = activityReference.get().findViewById(R.id.listView);
                    if (fragment instanceof HomeFragment) {
                        ((HomeFragment)fragment).refreshList(listView);
                    } else if (fragment instanceof DayFragment) {
                        ((DayFragment)fragment).refreshList(listView, Calendar.getInstance().getTime());
                    } else if (fragment instanceof EvaluationFragment) {
                        ((EvaluationFragment)fragment).refreshList(listView);
                    }
                }
            }
        }
    }
}


