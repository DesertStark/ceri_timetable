package com.project.ceri_timetable.webService;

import android.util.Log;

import com.project.ceri_timetable.model.VEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Classe statique qui contient des constantes pour le parseur
 */
class Names {
    public static final String DTSTART = "DTSTART";
    public static final String DTEND = "DTEND";
    public static final String SUMMARY = "SUMMARY";
    public static final String LOCATION = "LOCATION";
    public static final String DESCRIPTION = "DESCRIPTION";
}


public class IcalParser {
    private BufferedReader reader;
    private String currentLine = "";

    public IcalParser(BufferedReader reader) {
        this.reader = reader;
    }

    /**
     * Renvoie le prochain évènement présent dans le BufferedReader (le fichier ical) sous la forme d'un objet VEvent récupéré grace à readVEvent()
     */
    public VEvent nextVEvent() throws IOException {
        if (reader == null) {
            return null;
        }
        VEvent vEvent = null;
        while ((currentLine = reader.readLine()) != null) { // On parcours toutes les lignes du BufferedReader (le fichier ical)
            if (currentLine.startsWith("BEGIN:VEVENT")) { // Si on détecte un évènement ...
                vEvent = readVEvent(); // ... on le lit ...
                break; // ... puis on sort de la boucle
            }
        }
        return vEvent;
    }

    /**
     * Lis chaque ligne d'un évènement du BufferedReader (le fichier ical) et renvoie un objet VEvent
     */
    private VEvent readVEvent() throws IOException {
        VEvent vEvent = new VEvent();
        while ((currentLine = reader.readLine()) != null) { // On parcours toutes les lignes de l'évènement
            if (currentLine.startsWith("END:VEVENT")) { // Si on détecte la fin de l'évènement on sort de la boucle
                break;
            }
            // On split chaque ligne pour récupérer le nom et la valeur
            String name = currentLine.substring(0, currentLine.indexOf(":")).split(";")[0];
            String value = currentLine.substring(currentLine.indexOf(":")+1);
            // Selon le nom on va stocker l'information dans l'objet vEvent
            switch (name) {
                case Names.DTSTART:
                    vEvent.setDtStart(formatToDate(value));
                    break;
                case Names.DTEND:
                    vEvent.setDtEnd(formatToDate(value));
                    break;
                case Names.SUMMARY:
                    vEvent.setSummary(value);
                    break;
                case Names.LOCATION:
                    vEvent.setLocation(value);
                    break;
                case Names.DESCRIPTION:
                    vEvent.setName(getAttr("Matière", value));
                    vEvent.setDescription(value);
                    vEvent.setType(getType(value));
                    vEvent.setProf(getAttr("Enseignant", value));
                    break;
            }
        }
        return vEvent;
    }

    /**
     * Parse la description pour récupérer un attribut donné en paramètre
     */
    private String getAttr(String attr, String str) {
        String type = null;
        String[] splitted = str.split("\\\\n"); // On split la description pour récupérer chaque attribut
        for (String s : splitted) {
            String[] reSplitted = s.split(" : ");
            if (reSplitted[0].equals(attr)) {
                type = reSplitted[1];
            }
        }
        return type;
    }

    /**
     * Parse la description pour récupérer le type
     */
    private String getType(String str) {
        String type = null;
        String[] splitted = str.split("\\\\n");
        for (String s : splitted) {
            String[] reSplitted = s.split(" : ");
            if (reSplitted[0].equals("Type")) {
                type = reSplitted[1];
            }
        }
        return type;
    }

    /*public static Date toDate(String str) {
        String dateStr;
        String timeStr = null;
        if (str.contains("T")) {
            dateStr = str.substring(0, str.indexOf("T"));
            timeStr = str.substring(str.indexOf("T")+1, str.indexOf("Z"));
        } else {
            dateStr = str;
        }
        int year = Integer.parseInt(dateStr.substring(0, 4));
        int month = Integer.parseInt(dateStr.substring(4, 6))-1; // Months are 0 indexed like an array : 0 -> 11
        int day = Integer.parseInt(dateStr.substring(6, 8));
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, 0, 0, 0);
        if (timeStr != null) {
            hours = Integer.parseInt(timeStr.substring(0, 2));
            minutes = Integer.parseInt(timeStr.substring(2, 4));
            seconds = Integer.parseInt(timeStr.substring(4, 6));
        }
        cal = Calendar.getInstance();
        cal.set(year, month, day, hours, minutes, seconds);
        return cal.getTime();
    }*/

    /**
     * Formate la date contenu dans la String str donné en paramètre et la renvoi sous la forme d'un objet Date
     */
    public static Date formatToDate(String str) {
        SimpleDateFormat formatter;
        if (str.contains("T")) { // Si il y a la date et l'heure
            formatter = new SimpleDateFormat("yyyyMMdd'T'HHmmssZ", Locale.FRANCE);

        } else { // Si il n'y a que la date
            formatter = new SimpleDateFormat("yyyyMMdd", Locale.FRANCE);
        }
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(formatter.parse(str.replaceAll("Z$", "+0000")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal.getTime();
    }
}