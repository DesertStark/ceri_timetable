package com.project.ceri_timetable.webService;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {
    private static final String DEFAULT_URL = "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN";
    private static String url = "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN";
//    private static final String address = "https://accueil-ent2.univ-avignon.fr/edt/exportAgendaUrl?tdOptions=3394";
    private static final String HOST = "edt-api.univ-avignon.fr";
    private static final String PATH_1 = "app.php";
    private static final String PATH_2 = "api";
    private static final String PATH_3 = "exportAgenda";
    private static final String PATH_4 = "diplome";
    private static final String PATH_5 = "2-L3IN";

    public static URL build() throws MalformedURLException {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1)
                .appendPath(PATH_2)
                .appendPath(PATH_3)
                .appendPath(PATH_4)
                .appendPath(PATH_5);
        return new URL(builder.build().toString());
    }

    public static URL getURL() throws MalformedURLException {
        return new URL(WebServiceUrl.url);
    }

    public static String getUrlStr() {
        return url;
    }

    public static void setUrlStr(String url) {
        WebServiceUrl.url = url;
    }

    public static void resetUrl() {
        WebServiceUrl.url = WebServiceUrl.DEFAULT_URL;
    }
}
